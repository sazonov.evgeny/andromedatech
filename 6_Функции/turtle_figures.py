import turtle
import random
# Глобальная переменная t
t = turtle.Pen()
# Функция рисования звезды
def star(size):
    t.speed(0)
    t.pencolor("red")
    t.fillcolor("red")
    t.pendown()
    t.begin_fill()
    for i in range(5):
        t.forward(size)
        t.right(144)
    t.end_fill()
    t.penup()
# Функция рисования спирали
def spiral(size):
    t.speed(0)
    t.pencolor("blue")
    t.pendown()
    for i in range(16):
        t.forward(size)
        t.right(93)
    t.penup()
def random_pos():
    x = random.randint(-200, 200)
    y = random.randint(-200, 200)
    size = random.randint(10, 100)
    return x, y, size

# Основная программа
for i in range(20):
    x, y, size = random_pos()
    t.goto(x, y)
    star(size)
    
    x, y, size = random_pos()
    t.goto(x, y)
    spiral(size)
