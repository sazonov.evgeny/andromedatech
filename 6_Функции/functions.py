# Функции functions.py
# define - определить функцию
def hello(name="Человек"):
    print()
    print("Привет,", name, "!")
    print("*" * 15)

def square(a, b):
    s = a * b
    return s
    
# Вызов функции
room = square(4.5, 6)
tile = square(0.5, 0.5)
n = round(room / tile)
print(n)




