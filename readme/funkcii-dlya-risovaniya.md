# Функции для рисования

На примере использования черепашки, создадим несколько функций для рисования звездного пейзажа.

{% hint style="info" %}
Написать и протестировать программы можно в онлайн редакторе [https://pythonandturtle.com/turtle](https://pythonandturtle.com/turtle) или в мобильном приложении [Pydroid 3 (для Android)](https://play.google.com/store/apps/details?id=ru.iiec.pydroid3\&gl=US)
{% endhint %}

```python
import turtle, random

# Глобальная переменная t для простого обращения к черепашке
t = turtle.Turtle()

# Функция рисования звезды
def star(size, color):
    t.speed(0)
    t.pencolor(color)
    t.fillcolor(color)
    t.pendown()
    t.begin_fill()
    for i in range(5):
        t.forward(size)
        t.right(144)
    t.end_fill()
    t.penup()
    
# Функция рисования спирали
def spiral(size):
    t.speed(0)
    t.pencolor("green")
    t.pendown()
    for i in range(16):
        t.forward(size)
        t.right(99)
    t.penup()
    
# Функция генерации случайных величин    
def random_values():
    x = random.randint(-300, 300)
    y = random.randint(-300, 300)
    size = random.randint(5, 20)
    colors = ["red", "white", "blue"]
    color = random.choice(colors)
    return x, y, size, color

# Основная программа
turtle.bgcolor("black")
# Организуем цикл на 30 повторений
for i in range(30):
# Рисуем звезду случайного размера и цвета в случайном месте
    x, y, size, color = random_values()
    t.goto(x, y)
    star(size, color)
    
# Рисуем спираль случайного размера в случайном месте
    x, y, size, color = random_values()
    t.goto(x, y)
    spiral(size)
    
turtle.done()
```

<figure><img src="../.gitbook/assets/2023-01-27_17-41-33.png" alt=""><figcaption><p>Пример выполнения программы</p></figcaption></figure>

